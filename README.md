# BetterAny (Downcasting for Rust)

The basic idea is simple, this lets you write code like this

```rust
#[macro_use]
extern crate bany;

trait Message: bany::GetTypeId {}

impl Message for char {}

struct Quit;
impl Message for Quit {}

fn handle_messages<'a, I>(msgs: I)
where I: Iterator<Item=Box<Message>> {
    let mut out = String::new();
    for msg in msgs {

        // Match on the possible different types of `msg`.
        //
        // Stands for
        //  - dynamic (as opposed to the usual static)
        //  - reference (since we only match on `&X` types)
        //  - match
        //
        // `type` > `pattern` => `expression`,
        dyn_ref_match!(msg;
            char > &c => out.push(c),
            Quit > _ => break,
            else ()
        );
    }

    println!("{}", out);
}
```

Or more manually inspecting the types like this

```rust
impl Message for String {}
fn handle_messages_two<'a, I>(msgs: I)
where I: Iterator<Item=Box<Message>> {
    use bany::{Ext, BoxExt};

    for mut msg in msgs {

        // We just check the type here without downcasting
        if msg.is::<Quit>() {
            println!("");
            break;
        }

        // Cast `Box<Message>` to `Box<String>` if possible, otherwise
        // preserve ownership by assigning back to `msg`.
        match msg.downcast::<String>() {
            Ok(s) => {
                print!("{}", s);
                continue
            }
            Err(not_string) => msg = not_string
        }

        // This is the primitive the `dyn_ref_match` macro is built around,
        // attempt to downcast `&Message` to `&char`, returning a `Option<&char>`.
        if let Some(c) = msg.downcast_ref::<char>() {
            print!("{}", c);
        }
    }
}
```

The only requirement for types that we downcast is that they have a `'static`
lifetime. See `example/example.rs` for a few other ways to use this.

## Why?

The primary use case for this is "enums" (sum types) where you don't want to
list out every instance of the enum somewhere. Maybe just because that would
impose an undesired structure on your code. Maybe because you want to enable
foreign crates to add items to the enum. Or I suppose just because you don't
want to write `Char(char)` to wrap a foreign type in your enum.

## How?

Every type in your rust program is assigned a unique [`TypeId`](https://doc.rust-lang.org/std/any/struct.TypeId.html). We use this and traits
to  implement a downcast method that basically does

```rust
fn downcast_ref::<OtherType>(&self) -> Option<&OtherType> {
    if self.type_id() == OtherType::type_id() {
        Some(self as &OtherType)
    }
    else {
        None
    }
}
```

since this is a trait `self.type_id()` gets the original struct's type id, and
the downcast returns `Some(x)` iff `OtherType` is the original type of `self`.

Then we wrap some convenience macros around that. Do the same for `Box<T>`
except handling ownership, etc.

## Pitfalls

Mostly this just works, surprisingly enough. The only real pitfall I've run into
is that it's easy to accidentally have `self: &Box<T>` or some such type, and
have the type it is willing to downcast to be `Box<Trait>` instead of
`StructThatImplementsTrait`. This is solved by testing that the code successfully
downcasts, and if it doesn't dereferencing as necessary, e.g.
`(&*boxed_struct).downcast_ref::<OriginalStruct>()`.

## Prior work

This is very similar to Chris Morgan's [`mopa`](https://github.com/chris-morgan/mopa).
The primary difference being you don't need to `mopafy!` your traits in this version,
and that we provide the convenience `dyn_ref_match!` macro.

## Real world examples

I'm using this for a `Message` trait in my [text to speech based text editor](https://gitlab.com/gmorenz/speaking-editor)
to pass messages around to different components. E.g. see the code for saving and
opening files [here](https://gitlab.com/gmorenz/speaking-editor/blob/718c1f8a51f7a5494d1d4c231723bee0084cf9fa/src/aui/components.rs#L221).

I've also been using this code for awhile in a (not published, very WIPy) compiler
to make adding and removing new `Expression` and `Type` implementations easy.