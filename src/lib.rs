/// Boxed any downcasting

use std::any::TypeId;

pub trait GetTypeId {
    fn type_id(&self) -> TypeId;
}

impl <T: 'static> GetTypeId for T {
    fn type_id(&self) -> TypeId {
        TypeId::of::<Self>()
    }
}

pub unsafe trait Ext {
    fn is<O: 'static>(&self) -> bool;
    fn downcast_ref<O: 'static>(&self) -> Option<&O>;
    unsafe fn downcast_ref_unchecked<O: 'static>(&self) -> &O;
    fn downcast_ref_mut<O: 'static>(&mut self) -> Option<&mut O>;
    unsafe fn downcast_ref_mut_unchecked<O: 'static>(&mut self) -> &mut O;
}

unsafe impl <T: GetTypeId + ?Sized> Ext for T {
    fn is<O: 'static>(&self) -> bool {
        let tid = <T>::type_id(self);
        TypeId::of::<O>() == tid
    }

    fn downcast_ref<O: 'static>(&self) -> Option<&O> {
        if self.is::<O>() {
            unsafe{ Some(self.downcast_ref_unchecked()) }
        }
        else {
            None
        }
    }

    unsafe fn downcast_ref_unchecked<O>(&self) -> &O {
        let x: *const T = self;
        let y: *const O = x as *const O;
        let z: &O = y.as_ref().unwrap();
        z
    }

    fn downcast_ref_mut<O: 'static>(&mut self) -> Option<&mut O> {
        let b = {
            let x: &Self = self;
            x.is::<O>()
        };
        if b {
            unsafe{ Some(self.downcast_ref_mut_unchecked()) }
        }
        else {
            None
        }
    }

    unsafe fn downcast_ref_mut_unchecked<O: 'static>(&mut self) -> &mut O {
        let x: *mut T = self;
        let y: *mut O = x as *mut O;
        let z: &mut O = &mut *y;
        z
    }
}

pub trait BoxExt: Sized {
    fn box_is<O: 'static>(&self) -> bool;
    fn downcast<O: 'static>(self: Self) -> Result<Box<O>, Self>;
    unsafe fn downcast_unchecked<O>(self: Self) -> Box<O>;
}

impl <T: GetTypeId + ?Sized> BoxExt for Box<T> {
    fn box_is<O: 'static>(&self) -> bool {
        let tid = <T>::type_id(&**self);
        TypeId::of::<O>() == tid
    }

    fn downcast<O: 'static>(self: Box<T>) -> Result<Box<O>, Box<T>> {
        if self.box_is::<O>() {
            unsafe{ Ok(self.downcast_unchecked()) }
        }
        else {
            Err(self)
        }
    }

    unsafe fn downcast_unchecked<O>(self: Box<T>) -> Box<O> {
        Box::from_raw(Box::into_raw(self) as *mut O)
    }
}

#[macro_export]
macro_rules! dyn_ref_match {
    ( $x:ident;
        $T1:ty > $pat1:pat => $e1:expr,
        $($T:ty > $pat:pat => $e:expr,)*
        else $end:expr
    ) => {{
        use bany::Ext;

        // $x will in all useful use cases be a pointer, if it's an `&` pointer
        // the following `&*` will do nothing. Otherwise if we *didn't* do that
        // it would try and downcast_ref on the pointer type instead of the
        // contained type, e.g. if x: Box<Message>, then
        // x.downcast_ref::<Box<Message>>() works, and not anything else. To
        // work around that we use `&*` to get it to an `&` pointer form.
        if let Some(v) = (&* $x).downcast_ref::<$T1>() {
            let $pat1 = v;
            $e1
        }
        $(
            else if let Some(v) = (&* $x).downcast_ref::<$T>() {
                let $pat = v;
                $e
            }
        )*
        else {
            $end
        }
    }};

    // TODO: Test.
    ( $x:expr;
        $T1:ty > $pat1:pat => $e1:expr,
        $($T:ty > $pat:pat => $e:expr,)*
        else $end:expr
    ) =>  {{
        let v = $x;
        dyn_ref_match!{ v;
            $T1 > $pat1 => $e1,
            $($T > $pat => $e,)*
            else $end
        }
    }}
}


#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::Debug;

    #[derive(Debug)]
    struct A;
    #[derive(Debug)]
    struct B;
    trait C: GetTypeId + Debug {}
    impl C for A {}
    impl C for B {}

    #[test]
    fn box_downcast() {
        let c: Box<C> = Box::new(A);

        let b: Result<Box<B>, Box<C>> = c.downcast();
        assert!(b.is_err());

        let a: Result<Box<A>, Box<C>> = b.unwrap_err().downcast();
        assert!(a.is_ok());
    }

    #[test]
    fn downcast_ref() {
        let mut a = Box::new(A);
        let c: &mut C = &mut *a;
        // assert!(c.downcast_ref_mut::<A>().is_some());

        assert!(c.
        downcast_ref::<B>().is_none());
        // assert!(c.downcast_ref::<A>().is_some());
        assert!(c.downcast_ref_mut::<B>().is_none());
    }

    // #[test]
    // fn map() {
    //     let mut m = Map::new();
    //     assert!(m.get::<A>().is_none());
    //     assert!(m.set(A).is_none());
    //     assert!(m.set(A).is_some());
    //     assert!(m.get::<A>().is_some());
    // }
}