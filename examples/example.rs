#[macro_use]
extern crate bany;

// Example 1, direct GetTypeId usage, raw methods

fn print_u8s(vals: &[&bany::GetTypeId]) {
    use bany::Ext;

    for &val in vals {
        if let Some(x) = val.downcast_ref::<u8>() {
            println!("{}", x);
        }
    }
}

// Example 2, direct GetTypeId usage, convenience macro

fn print_ints(vals: &[&bany::GetTypeId]) {
    for &val in vals {
        dyn_ref_match!(val;
            u8 > &x => println!("{}", x),
            u16 > &x => println!("{}", x),
            u32 > &x => println!("{}", x),
            u64 > &x => println!("{}", x),
            usize > &x => println!("{}", x),
            i8 > &x => println!("{}", x),
            i16 > &x => println!("{}", x),
            i32 > &x => println!("{}", x),
            i64 > &x => println!("{}", x),
            isize > &x => println!("{}", x),
            else println!("not an int")
        )
    }
}

// Example 3, trait that depends on GetTypeId (This is how I typically use this)

trait Message: bany::GetTypeId {}

impl Message for char {}

struct Quit;
impl Message for Quit {}

fn handle_messages<'a, I>(msgs: I)
where I: Iterator<Item=Box<Message>> {
    let mut out = String::new();
    for msg in msgs {
        dyn_ref_match!(msg;
            char > &c => out.push(c),
            Quit > _ => break,
            else ()
        );
    }

    println!("{}", out);
}

// Example 4, destructuring boxes

impl Message for String {}
fn handle_messages_two<'a, I>(msgs: I)
where I: Iterator<Item=Box<Message>> {
    use bany::{Ext, BoxExt};

    for mut msg in msgs {
        if msg.is::<Quit>() {
            println!("");
            break;
        }

        match msg.downcast::<String>() {
            Ok(s) => {
                // We take ownership of s here, no new allocation!
                print!("{}", s);
                continue
            }
            // Preserve ownership of non string messages
            Err(not_string) => msg = not_string
        }

        if let Some(c) = msg.downcast_ref::<char>() {
            print!("{}", c);
        }
    }
}


fn main() {
    use std::iter::once;

    print_u8s(&[&10u8, &32i32, &vec!["some complex structure"]]);
    print_ints(&[&1i32, &2u8, &"cat", &"cat".len()]);

    let messages = "Hello world!".chars()
        .map(|x| Box::new(x) as Box<Message>)
        .chain(once(Box::new(Quit) as Box<Message>))
        .chain("Quit before here".chars()
                        .map(|x| Box::new(x) as Box<Message>));

    handle_messages(messages);

    let messages_two = "Hello again!".chars()
        .map(|x| Box::new(x) as Box<Message>)
        .chain(once(Box::new("\nA string".to_string()) as Box<Message>))
        .chain(once(Box::new(Quit) as Box<Message>))
        .chain("Quit before here".chars()
                        .map(|x| Box::new(x) as Box<Message>));

    handle_messages_two(messages_two);
}